/*************************
  ReservOpStatusTblDAO.js
 *************************/

/** AWS */
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient();

require("date-utils");

// TODO: 最終的にはEntityっぽく使う実装にしたい

// class ReservOpStatusTblEntity{
//     constructor(userId, operationType, menuId, reservationDate, reservationTime){
//         var dt = new Date();
//         this.userId = userId;
//         this.operationType = operationType;
//         this.menuId = menuId;
//         this.reservationDate = reservationDate;
//         this.reservationTime = reservationTime;
//         this.operationDateTime = dt.toFormat("YYYY/MM/DD HH24:MI");
//     }
// }

// TODO: 【全体】店舗IDを追加（一人が複数店舗で同時に予約操作をする可能性があるため）

/**
 * 予約操作ステータステーブルに指定されたユーザーのレコードを追加
 * 
 * @param userId        LINEのユーザーID
 * @param operationType 操作の種類（1：予約、2：予約変更、3:キャンセル）
 * @param userName      LINEのユーザー名
 * @return 終了ステータス（true : 成功、false : 失敗）
 */
exports.add = async function (userId, operationType, userName) {
    return new Promise((resolve, reject) => {
        var date = new Date();
        // 予約操作ステータステーブルに追加するデータを作成
        var params = {
            TableName: "Session_TBL",
            Item: {
                "UserID": userId,
                "UserName": userName,
                "OperationType": operationType,
                "MenuID": null,
                "RequiredTimeHours": null,
                "RequiredTimeMinutes": null,
                "ReservationDate": null,
                "ReservationTime": null,
                "OperationDateTime": date.toFormat("YYYY/MM/DD HH24:MI")
            }
        };

        // 予約操作ステータステーブルのレコード追加処理
        var addFunc = async function (params) {
            return new Promise((resolve, reject) => {
                docClient.put(params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        }

        // 予約操作ステータステーブルのレコード追加処理を実行
        addFunc(params)
            .then(() => {
                console.log("予約操作ステータスの追加に成功しました。");
                resolve();
            })
            .catch((errMsg) => {
                console.error("予約操作ステータスの追加に失敗しました。 Error JSON:", JSON.stringify(errMsg, null, 2));
                reject();
            });
    })
}

/**
 * 予約操作ステータステーブルの指定されたユーザーのレコードを削除
 * 
 * @param userId        LINEのユーザーID
 * @return 終了ステータス（true : 成功、false : 失敗）
 */
exports.delete = async function (userId) {
    return new Promise((resolve, reject) => {
        var params = {
            TableName: "Session_TBL",
            Key: {
                "UserID": userId
            }
        };

        // レコード削除処理
        var deleteFunc = async function (params) {
            return new Promise((resolve, reject) => {
                docClient.delete(params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        }

        // レコード削除処理を実行
        deleteFunc(params)
            .then(() => {
                console.log("予約操作ステータスの削除に成功しました。");
                resolve();
            })
            .catch((errMsg) => {
                console.error("予約操作ステータスの削除に失敗しました。 Error JSON:", JSON.stringify(errMsg, null, 2));
                reject();
            });
    });
}

/**
 * 予約操作ステータステーブルの指定されたパラメータの通りレコードを更新
 * 
 * @param params        更新情報パラメータ
 * @return 終了ステータス（true : 成功、false : 失敗）
 */
exports.update = async function (params) {
    return new Promise((resolve, reject) => {
        var updateFunc = async function (params) {
            return new Promise((resolve, reject) => {
                // TODO: 他と同じようにユーザーID、更新内容配列 [ カラム名、値 ]を渡してパラメータを自動生成できるようにする
                // 予約操作ステータステーブルのデータを更新
                docClient.update(params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        }

        // レコード更新処理を実行
        updateFunc(params)
            .then(() => {
                console.log("予約操作ステータスの更新に成功しました。");
                resolve();
            })
            .catch((errMsg) => {
                console.error("予約操作ステータスの更新に失敗しました。 Error JSON:", JSON.stringify(errMsg, null, 2));
                reject();
            });
    });
}

/**
 * 予約操作ステータステーブルの指定された条件に該当するデータを取得
 * 
 * @param params        取得条件パラメータ
 * @return 取得したデータ（失敗した場合はnull）
 */
exports.select = async function (params) {
    return new Promise((resolve, reject) => {
        // TODO: 他と同じようにユーザーID、更新内容配列 [ カラム名、値 ]を渡してパラメータを自動生成できるようにする

        var selectFunc = async function (params) {
            return new Promise((resolve, reject) => {
                docClient.query(params, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                })
            })
        }

        // レコード更新処理を実行
        selectFunc(params)
            .then((data) => {
                console.log("予約操作ステータスの取得に成功しました。", JSON.stringify(data, null, 2));
                resolve(data);
            })
            .catch((errMsg) => {
                console.error("予約操作ステータスの取得に失敗しました。 Error JSON:", JSON.stringify(errMsg, null, 2));
                reject();
            });
    });



    // // レコード更新処理を実行
    // let selectResult = await selectFunc().catch((errMsg) => {
    //     console.error("予約操作ステータスの取得に失敗しました。 Error JSON:", JSON.stringify(errMsg, null, 2));
    //     return null;
    // });
    // console.log("予約操作ステータスの取得に成功しました。", JSON.stringify(selectResult, null, 2));
    // return selectResult;
}