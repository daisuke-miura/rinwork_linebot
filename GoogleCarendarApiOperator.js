/*************************
  GoogleCarendarApiOperator.js
 *************************/

/**
 * パッケージのインスタンス生成
 */
var Calendar = require('node-google-calendar');
var Config = require('./credentials/calendar-config');
// TODO: カレンダーIDを店ごとに切り替える
var CalendarId = Config.calendarId.myCal;

var cal = new Calendar(Config);

/**
 * 日付からカレンダーイベントを取得
 * 
 * @param userId        LINEのユーザーID
 * @param date          取得対象の日付
 * @param openTime      取得対象の開始時間
 * @param closeTime      取得対象の終了時間
 * @return Promiseオブジェクト
 *          resolve : 指定した期間の予約リスト
 *          reject  : エラー内容
 */
exports.getCalEventByDateTime = async function (userId, date, openTime, closeTime) {
    return new Promise((resolve, reject) => {
        // var dateString = date.getFullYear() + "-"
        //     + date.getMonth() + "-" + date.getDate();
        var openTimeString =
            (("0" + openTime.getHours()).slice(-2)) + ":"
            + (("0" + openTime.getMinutes()).slice(-2)) + ":"
            + (("0" + openTime.getSeconds()).slice(-2));
        var closeTimeString =
            (("0" + closeTime.getHours()).slice(-2)) + ":"
            + (("0" + closeTime.getMinutes()).slice(-2)) + ":"
            + (("0" + closeTime.getSeconds()).slice(-2));

        console.log("openTimeString", openTimeString);
        console.log("closeTimeString", closeTimeString);
        // 取得する対象期間を指定
        // timeMinからtimeMaxの間のカレンダーイベントが取得される
        var params = {
            // timeMin: '2021-01-28T06:00:00+09:00',
            // timeMax: '2021-01-31T06:00:00+09:00',
            timeMin: date + 'T' + openTimeString + '+09:00',
            timeMax: date + 'T' + closeTimeString + '+09:00'
        }
        console.log("params", params);

        // TODO: userIdから、店舗、担当者のカレンダーIDを特定して引数に渡すよう修正
        cal.Events.list(CalendarId, params)
            .then(calEvents => {
                console.log("calEvents", calEvents);
                resolve(calEvents);
            })
            .catch(err => {
                console.log(err.message);
                reject(err);
            });
    })
}

/**
 * LINE名からカレンダーイベントを取得
 * 
 * @param userName        LINEのユーザー名
 * @return Promiseオブジェクト
 *          resolve : 指定したユーザーの予約リスト
 *          reject  : エラー内容
 */
exports.getCalEventByLineName = async function (userName) {
    return new Promise((resolve, reject) => {
        var dt = new Date();
        dt.setDate(dt.getDate() + 1);

        // 取得するユーザー名を指定
        var params = {
            summary: userName,
            timeMin: dt.toFormat("YYYY-MM-DD") + "T00:00:00+00:00"
        }
        cal.Events.list(CalendarId, params)
            .then(calEvents => {
                console.log("calEvents", calEvents);
                resolve(calEvents);
            })
            .catch(err => {
                console.log(err.message);
                reject(err);
            });
    })
}

/**
 * カレンダーイベントの追加
 * 
 * @param addData        追加するイベントの情報（セッションテーブルのレコード）
 * @return Promiseオブジェクト
 *          resolve : なし
 *          reject  : なし
 */
exports.addCalEvent = async function (addData) {
    return new Promise((resolve, reject) => {
        console.log("★addData[Items]★");
        console.log(JSON.stringify(addData));

        if (!addData) {
            console.log("カレンダーに追加するイベント情報がセットされていません");
            reject();
        }

        // 施術終了時間の計算
        let endTime = new Date();
        endTime.setHours(addData["Items"][0]["ReservationTime"].split(":")[0]);
        endTime.setMinutes(addData["Items"][0]["ReservationTime"].split(":")[1]);
        endTime.setHours(endTime.getHours() + addData["Items"][0]["RequiredTimeHours"]);
        endTime.setMinutes(endTime.getMinutes() + addData["Items"][0]["RequiredTimeMinutes"]);

        console.log("施術終了時間", endTime);

        // TODO : カレンダーに追加するイベント名を、予約者名、プラン名がわかるようなイベント名に変更する
        var event = {
            "start": { "dateTime": addData["Items"][0]["ReservationDate"] + "T" + addData["Items"][0]["ReservationTime"] + ":00+09:00" },
            "end": { "dateTime": addData["Items"][0]["ReservationDate"] + "T" + endTime.getHours() + ":" + endTime.getMinutes() + ":00+09:00" },
            "summary": addData["Items"][0]["UserName"],
            "description": addData["Items"][0]["MenuID"],
            "colorId": 1,   // 1:ラベンダ(薄紫)
        };

        // console.log("★calId★");
        // console.log(CalendarId);
        // console.log("★event★");
        // console.log(event);

        cal.Events.insert(CalendarId, event)
            .then(resp => {
                console.log(resp);
                resolve();
            })
            .catch(err => {
                console.log(err.message);
                reject();
            });
    });
}

/**
 * カレンダーイベントの削除
 * 
 * @param lineName        LINEのユーザー名
 * @return Promiseオブジェクト
 *          resolve : なし
 *          reject  : なし
 */
exports.deleteCalEventByLineName = function (lineName) {
    return new Promise((resolve, reject) => {
        // 削除対象の予約の検索用パラメータ
        var params = {
            summary: lineName
        }
        console.log("★★★カレンダーイベント削除★★★");
        // 削除対象の予約を取得
        cal.Events.list(CalendarId, params)
            // 取得した予定のイベントIDを使って予定を削除
            .then((calEvents) => {
                console.log("削除時calEvents", JSON.stringify(calEvents));
                if(calEvents.length > 0){
                    delCalEventByEventId(calEvents[0]["id"]);
                }
            })
            .then(() => resolve())
            .catch(err => {
                console.log(err.message);
                reject();
            })
    })
    // calEvents.forEach(event => {
    //     // タイトルに「hoge」の文字列が含まれるイベントを検索
    //     if (/hoge/.test(event.summary)) {
    //         // イベントIDを取得
    //         var eventId = event.id;
    //         // イベントの削除
    //         delCalEventByEventId(eventId);
    //     }
    // });
}

/**
 * イベントの削除
 * 
 * @param {*} eventId 
 */
function delCalEventByEventId(eventId) {
    return new Promise((resolve, reject) => {
        console.log("削除時eventId", eventId);
        cal.Events.delete(CalendarId, eventId)
        .then(results => {
            console.log(JSON.stringify(results));
            resolve();
        })
        .catch(err => {
            console.log(err.message);
            reject();
        });
    })
}