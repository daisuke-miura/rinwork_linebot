//　----------------------------------------------------------------------
// TODO : 
//　　＜全体＞
//      ・エラー時のログを整備
//
//　----------------------------------------------------------------------




// const dateUtils = require("date-utils")

// /** LINE */
// const line = new require("@line/bot-sdk");
// const client = new line.Client({
//     channelAccessToken: process.env.ACCESS_TOKEN
// });

/** LINEに返すメッセージ生成 */
const replyJsonGenerator = require("./ReplyJsonGenerator")

/** GoogleCalendarAPI操作 */
const GCAOperator = require("./GoogleCarendarApiOperator")

/** 予約操作ステータステーブルDAO */
const reservOpStatusTblDAO = require("./ReservOpStatusTblDAO")

/** Line操作 */
const lineOperator = require("./LineOperator")

/** スケジュール計算 */
const scheduleCalculator = require("./ScheduleCalculator")


// --- 定数 ---
/** 予約 */
const OPERATION_TYPE_NEW_RESERV = "予約"
/** 予約変更 */
const OPERATION_TYPE_CHANGE_RESERV = "予約変更"
/** キャンセル */
const OPERATION_CANCEL = "キャンセル"
// -----------

/**
 * LINEbotに何か送られてきたときのハンドラー
 */
exports.handler = function (req, context) {
    console.log(req);

    // TODO: 署名(X-Line-Signature)の検証処理を追加
    // https://developers.line.biz/ja/reference/messaging-api/#signature-validation

    var body = JSON.parse(req.body);                  // リクエストのbodyをJSONにする 
    var replyToken = body["events"][0]["replyToken"];       // 返信する時に必要なトークン
    var userId = body["events"][0]["source"]["userId"]; // ユーザIDを取得

    // 友達追加された時（Follow Event）
    if (body["events"][0]["type"] == "follow") {
        // TODO: ユーザーの氏名を登録させる or 美容師側が後で登録する前提で一旦LINE名でユーザーテーブルに登録


    }
    // Message Event(テキスト)が送られてきたとき    
    else if (body["events"][0]["type"] == "message") {

        // 送られてきたテキストメッセージを取得
        var receivedText = body["events"][0]["message"]["text"];

        if (receivedText === "予約") {
            // TODO : 既に予約を入れている場合、確認の上、上書きするよう修正(一人につき予約は１つ)
            Promise.all([
                // 予約ステータステーブルにデータがあったら削除
                reservOpStatusTblDAO.delete(userId),
                // LINEユーザー名を取得
                lineOperator.getLineName(userId)
            ]).then((values) => {
                // 削除が完了したら予約操作ステータステーブルへのレコード追加を実行
                reservOpStatusTblDAO.add(userId, OPERATION_TYPE_NEW_RESERV, values[1]);
            }).then(() => {
                // レコード追加が完了したらメニュー選択用メッセージを送信
                let repJson = replyJsonGenerator.getSelectMenuJson(replyToken);
                lineOperator.sendReplyMessage(repJson);
                return;
            }).catch(() => {
                // エラーが発生したら最初から操作しなおす旨のメッセージを送信
                let repJson = replyJsonGenerator.getErrorJson(replyToken);
                lineOperator.sendReplyMessage(repJson);
                return;
            });
        }
        else if (receivedText === "予約確認" || receivedText === "予約変更" || receivedText === "キャンセル") {
            // LINEユーザー名を取得
            lineOperator.getLineName(userId)
                // LINEユーザー名に紐づく予約情報を取得
                .then((lineName) => GCAOperator.getCalEventByLineName(lineName))
                .then((calEvents) => {
                    // メッセージを送ったユーザーからの予約がなかった場合
                    if (!calEvents.length) {
                        // 予約なしメッセージを送信
                        var repJson = replyJsonGenerator.getNoReservationJson(replyToken);
                    } 
                    // メッセージを送ったユーザーからの予約があった場合
                    else {
                        // 予約開始、終了日時を取得
                        let reservStartDateTime = new Date(calEvents[0]["start"]["dateTime"]);
                        // 日本時間に補正
                        reservStartDateTime.setHours(reservStartDateTime.getHours() + 9);

                        // メニュー名取得
                        let menuId = calEvents[0]["description"];
                        // TODO : テーブルから取得するようにする（店ID ＋ メニューID）
                        if (menuId === "cut") {
                            var menuName = "\nrin.のカットトリートメント";
                        } else if (menuId === "color") {
                            var menuName = "\n髪本来の綺麗を取り戻す\nカラートリートメント";
                        } else if (menuId === "perm") {
                            var menuName = "\n髪本来の綺麗を取り戻す\nパーマトリートメント";
                        } else if (menuId === "straight") {
                            var menuName = "\n髪本来の綺麗を取り戻す\nストレートトリートメント";
                        }

                        if (receivedText === "予約確認") {
                            // 予約内容確認メッセージを送信
                            var repJson = replyJsonGenerator.getCheckReservationJson(replyToken, reservStartDateTime, menuName);
                        } else if (receivedText === "予約変更") {
                            // 予約変更確認メッセージを送信
                            var repJson = replyJsonGenerator.getChangePermissionJson(replyToken, reservStartDateTime, menuName);
                        } else if (receivedText === "キャンセル") {
                            // 予約キャンセル確認メッセージを送信
                            var repJson = replyJsonGenerator.getCancelPermissionJson(replyToken, reservStartDateTime, menuName);
                        }
                    }

                    lineOperator.sendReplyMessage(repJson);
                    return;
                }).catch((err) => {
                    // エラーが発生したら最初から操作しなおす旨のメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    console.log("Error:", err);
                    return;
                })
        }
    } else if (body["events"][0]["type"] == "postback") {
        // "data"に入っている情報を取得
        var receivedData = body["events"][0]["postback"]["data"];

        // 予約変更操作継続の場合
        if (receivedData === "reservation_change") {
            // TODO : 予約機能と全く同じ処理なので、共通化する
            Promise.all([
                // 予約ステータステーブルにデータがあったら削除
                reservOpStatusTblDAO.delete(userId),
                // LINEユーザー名を取得
                lineOperator.getLineName(userId)
            ]).then((values) => {
                // 削除が完了したら予約操作ステータステーブルへのレコード追加を実行
                reservOpStatusTblDAO.add(userId, OPERATION_TYPE_CHANGE_RESERV, values[1]);
            }).then(() => {
                // レコード追加が完了したらメニュー選択用メッセージを送信
                let repJson = replyJsonGenerator.getSelectMenuJson(replyToken);
                lineOperator.sendReplyMessage(repJson);
                return;
            }).catch(() => {
                // エラーが発生したら最初から操作しなおす旨のメッセージを送信
                let repJson = replyJsonGenerator.getErrorJson(replyToken);
                lineOperator.sendReplyMessage(repJson);
                return;
            });
        }
        else if (receivedData === "cut" || receivedData === "color" || receivedData === "perm" || receivedData === "straight") {
            // TODO : 店舗ごとの各メニューの目安所要時間を管理するテーブルを作成し、
            //　　　　 予約ステータステーブルに設定するようにする。今は一旦固定値
            let requireTimeHours = 2;
            let requireTimeMinutes = 0;

            // 予約ステータステーブルの更新情報を作成
            var params = {
                TableName: "Session_TBL",
                Key: {
                    "UserID": userId
                },
                UpdateExpression: 'set MenuID = :menu_id, RequiredTimeHours = :required_time_hours, RequiredTimeMinutes = :required_time_minutes',
                ExpressionAttributeValues: {
                    ':menu_id': receivedData,
                    ':required_time_hours': requireTimeHours,
                    ':required_time_minutes': requireTimeMinutes
                }
            };
            // 予約ステータステーブルにデータを更新
            reservOpStatusTblDAO.update(params)
                .then(() => {
                    // 日付選択用メッセージを送信
                    let repJson = replyJsonGenerator.getSelectDateJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                })
                .catch(() => {
                    // エラーメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        else if (receivedData === "setdate") {
            // 予約ステータステーブルの更新情報を作成
            var reservationDate = body["events"][0]["postback"]["params"]["date"];
            console.log("reservationDate", reservationDate);
            var params = {
                TableName: "Session_TBL",
                Key: {
                    "UserID": userId
                },
                UpdateExpression: 'set ReservationDate = :reservation_date',
                ExpressionAttributeValues: {
                    ':reservation_date': reservationDate
                }
            };

            Promise.all([
                // 予約可能時間を取得
                scheduleCalculator.getReservableTimeList(userId, reservationDate),
                // 予約ステータステーブルのデータを更新
                reservOpStatusTblDAO.update(params)
            ]).then((values) => {
                console.log("values", values);
                // 予約時間選択メッセージを送信（選択肢が13個以上ある場合は前半の時間帯を表示）
                var repJson = replyJsonGenerator.getSelectTimeJson(replyToken, values[0]);
                console.log("repJson", repJson);
                lineOperator.sendReplyMessage(repJson);
                return;
            }).catch(() => {
                // エラーメッセージを送信
                let repJson = replyJsonGenerator.getErrorJson(replyToken);
                lineOperator.sendReplyMessage(repJson);
                return;
            });
        }
        else if (receivedData === "settime_prev" || receivedData === "settime_next") {
            // 予約ステータステーブルからデータ取得用パラメータ
            var params = {
                TableName: "Session_TBL",
                KeyConditionExpression: '#userId = :val',
                ExpressionAttributeNames: { '#userId': 'UserID' },
                ExpressionAttributeValues: { ':val': userId }
            };
            // 予約ステータステーブルにデータを取得
            reservOpStatusTblDAO.select(params)
                // 予約可能時間を取得
                .then(selectedData => scheduleCalculator.getReservableTimeList(userId, selectedData["Items"][0]["ReservationDate"]))
                .then((values) => {
                    console.log("values", values);
                    // 日付選択後、又は時間選択で"←"ボタンが押下された場合
                    if (receivedData === "settime_prev") {
                        // 予約時間選択メッセージを送信（選択肢が13個以上ある場合は前半の時間帯を表示）
                        var repJson = replyJsonGenerator.getSelectTimePrevJson(replyToken, values);
                        console.log("repJson_prev", JSON.stringify(repJson));
                        lineOperator.sendReplyMessage(repJson);
                        return;
                    }
                    // 時間選択で"→"ボタンが押下された場合（選択肢が13個以上ある場合）
                    else if (receivedData === "settime_next") {
                        // 予約時間選択メッセージを送信（選択肢が13個以上ある場合は前半の時間帯を表示）
                        var repJson = replyJsonGenerator.getSelectTimeNextJson(replyToken, values);
                        console.log("repJson_Next", JSON.stringify(repJson));
                        lineOperator.sendReplyMessage(repJson);
                        return;
                    }
                }).catch(() => {
                    // エラーメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        else if (receivedData.startsWith("settime")) {
            // 予約ステータステーブルの更新情報を作成
            var reservationTime = receivedData.split("_")[1];       // TODO: ★splitできなかった場合の例外処理追加
            var params = {
                TableName: "Session_TBL",
                Key: {
                    "UserID": userId
                },
                UpdateExpression: 'set ReservationTime = :reservationTime',
                ExpressionAttributeValues: {
                    ':reservationTime': reservationTime
                }
            };

            // 予約ステータステーブルにデータを更新
            reservOpStatusTblDAO.update(params)
                .then(() => {
                    return new Promise((resolve, reject) => {
                        // 予約ステータステーブルからデータ取得用パラメータ
                        var params = {
                            TableName: "Session_TBL",
                            KeyConditionExpression: '#userId = :val',
                            ExpressionAttributeNames: { '#userId': 'UserID' },
                            ExpressionAttributeValues: { ':val': userId }
                        };
                        // 予約ステータステーブルにデータを取得
                        reservOpStatusTblDAO.select(params)
                            .then((selectedData) => {
                                resolve(selectedData);
                            })
                            .catch(() => {
                                reject();
                            });
                    })
                })
                .then((selectedData) => {
                    // 予約日時取得
                    let reservationDate = selectedData["Items"][0]["ReservationDate"];
                    let reservationTime = selectedData["Items"][0]["ReservationTime"];

                    // 操作名取得
                    let operationType = selectedData["Items"][0]["OperationType"];

                    // メニュー名取得
                    // TODO : テーブルから取得するようにする（店ID ＋ メニューID）
                    var menuName;
                    if (selectedData["Items"][0]["MenuID"] === "cut") {
                        menuName = "カットトリートメント";
                    } else if (selectedData["Items"][0]["MenuID"] === "color") {
                        menuName = "カラートリートメント";
                    } else if (selectedData["Items"][0]["MenuID"] === "perm") {
                        menuName = "パーマトリートメント";
                    } else if (selectedData["Items"][0]["MenuID"] === "straight") {
                        menuName = "ストレートトリートメント";
                    }
                    // 予約完了確認メッセージを送信
                    let repJson = replyJsonGenerator.getConfirmationJson(replyToken, operationType, reservationDate, reservationTime, menuName);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                })
                .catch(() => {
                    // エラーメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        // ※予約変更完了時もここを通る
        else if (receivedData === "reservation_completed") {
            // 予約ステータステーブルからデータ取得用パラメータ
            var params = {
                TableName: "Session_TBL",
                KeyConditionExpression: '#userId = :val',
                ExpressionAttributeNames: { '#userId': 'UserID' },
                ExpressionAttributeValues: { ':val': userId }
            };

            // TODO : ★★★ 既に予約を入れている場合、消してから予約を追加 ★★★
            // LINEユーザー名を取得
            lineOperator.getLineName(userId)
                // LINEユーザー名に紐づく予定を削除
                .then((lineName) => GCAOperator.deleteCalEventByLineName(lineName))
                // 予約ステータステーブルからデータを取得
                .then(() => reservOpStatusTblDAO.select(params))
                // GoogleCalendarAPIを呼んでイベントを追加
                .then((selectedData) => GCAOperator.addCalEvent(selectedData))
                // イベント追加成功
                .then(() => {
                    // 予約完了メッセージを送信
                    let repJson = replyJsonGenerator.getCompleteJson(replyToken, "予約");
                    lineOperator.sendReplyMessage(repJson);
                    return;
                })
                .catch(() => {
                    // エラーメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        // キャンセル完了の場合
        else if (receivedData === "reservation_cancel_completed") {
            Promise.all([
                // 予約ステータステーブルにデータがあったら削除
                reservOpStatusTblDAO.delete(userId),
                // LINEユーザー名を取得
                lineOperator.getLineName(userId)
            ])
                // LINEユーザー名に紐づく予定を削除
                .then((lineName) => GCAOperator.deleteCalEventByLineName(lineName))
                .then(() => {
                    // キャンセル完了メッセージを送信
                    let repJson = replyJsonGenerator.getCompleteJson(replyToken, "キャンセル");
                    lineOperator.sendReplyMessage(repJson);
                    return;
                }).catch(() => {
                    // エラーが発生したら最初から操作しなおす旨のメッセージを送信
                    let repJson = replyJsonGenerator.getErrorJson(replyToken);
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        else if (receivedData === "reservation_suspend") {
            // 予約ステータステーブルにデータがあったら削除
            reservOpStatusTblDAO.delete(userId)
                .then(() => {
                    // 予約をキャンセルした旨のメッセージを送信
                    let repJson = replyJsonGenerator.getSuspensionJson(replyToken, "予約");
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        else if (receivedData === "reservation_change_suspend") {
            // 予約ステータステーブルにデータがあったら削除
            reservOpStatusTblDAO.delete(userId)
                .then(() => {
                    // 予約変更をキャンセルした旨のメッセージを送信
                    let repJson = replyJsonGenerator.getSuspensionJson(replyToken, "予約変更");
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        else if (receivedData === "reservation_cancel_suspend") {
            // 予約ステータステーブルにデータがあったら削除
            reservOpStatusTblDAO.delete(userId)
                .then(() => {
                    // 予約変更をキャンセルした旨のメッセージを送信
                    let repJson = replyJsonGenerator.getSuspensionJson(replyToken, "キャンセル");
                    lineOperator.sendReplyMessage(repJson);
                    return;
                });
        }
        // リッチメニュー切り替え（テスト）
        else if (receivedData === "changeTab_1") {
            lineOperator.changeRichMenu(userId, "richmenu-8132c29a9fb69bfd0875a4b11849f442");
        }
        // リッチメニュー切り替え（テスト）
        else if (receivedData === "changeTab_2") {
            lineOperator.changeRichMenu(userId, "richmenu-c6460b5e765c1dd96276bf936e92c548");
        }
        else {
            // postbackで受け取った文字列がどのパターンにも該当しない場合、
            //　不正な入力と判断し、メニューから操作する旨のメッセージを送信
            let repJson = replyJsonGenerator.getIllegalOperationJson(replyToken);
            lineOperator.sendReplyMessage(repJson);
            return;
        }
    }
}