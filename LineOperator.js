/*************************
  LineOpUtil.js
 *************************/

const request = require("request");

/** LINE チャネルアクセストークン */
const lineAccessToken = process.env.ACCESS_TOKEN   // Lambdaの環境変数に設定している
// var channelAccessToken = "+4l9xWXSTWPgHeDnCr/zgH1AQTbkk+EOQPltZdUUiZPqKd7UhasgOOaFE7/EiiM19pa623RgMmgVMMKFay5L7wkRmWWXhk17knsXWRo1Xk5cWSrpJcixsu+8CpghoCPBV7nUMNvK7XSqtoeDYAx9ygdB04t89/1O/w1cDnyilFU=";

const line = require('@line/bot-sdk');

var richMenu = {
    Reservation : "richmenu-8132c29a9fb69bfd0875a4b11849f442",
    Test        : "richmenu-c6460b5e765c1dd96276bf936e92c548"
}

/**
 * メッセージを返す
 */
exports.sendReplyMessage = async function (sendData) {
    // ヘッダー定義
    // AuthorizationにはbotそれぞれのChannel Access Tokenを設定(LINE developersで確認できる)
    var headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {" + lineAccessToken + "}",
    };

    // オプション定義
    // 返事をするだけならbodyにSendDataを設定するだけで、他はいじらなくてOK
    var options = {
        url: "https://api.line.me/v2/bot/message/reply",    // LINEbotの返事をしてくれるAPI
        headers: headers,                                   // 上で設定したヘッダー
        json: true,
        body: sendData
    };

    // ユーザへメッセージの送信
    request.post(options, (error, response, body) => {
        // 送信成功の場合
        if (!error && response.statusCode == 200) {
            console.log("LINE応答に成功しました。", body);  // コンソールに何を送ったか確認出力
        // 送信失敗の場合
        } else {
            console.log("LINE応答に失敗しました。", JSON.stringify(response));  // コンソールにエラーメッセージを確認出力
        }
    });
};

/**
 * Line名を取得
 */
exports.getLineName = async function (userId) {
    return new Promise((resolve, reject) => {
        // ヘッダー定義
        // AuthorizationにはbotそれぞれのChannel Access Tokenを設定(LINE developersで確認できる)
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {" + lineAccessToken + "}",
        };

        // オプション定義
        // 返事をするだけならbodyにSendDataを設定するだけで、他はいじらなくてOK
        var options = {
            url: 'https://api.line.me/v2/bot/profile/' + userId,
            headers: headers,
            json: true
        };

        // ユーザへメッセージの送信
        request.get(options, (request, response, next) => {
            // 送信成功の場合
            if (response.statusCode == 200) {
                console.log(response.body);
                resolve(response.body.displayName);
            // 送信失敗の場合
            } else {
                reject();
            }
        });
    })
}

/**
 * リッチメニュー切り替え
 */
exports.changeRichMenu = function (userId, richMenuName){
    // var headers = {
    //     "Authorization": "Bearer {" + lineAccessToken + "}"
    // }
    // var options = {
    //     url: 'https://api.line.me/v2/bot/user/' + userId + '/richmenu/' + richMenu[richMenuName],
    //     headers: headers,
    //     json: true,
    //     body: {}
    // };

    // // ユーザへメッセージの送信
    // request.post(options, (error, response, body) => {
    //     // 送信成功の場合
    //     if (!error && response.statusCode == 200) {
    //         console.log("リッチメニュー切り替えに成功しました。", body);  // コンソールに何を送ったか確認出力
    //     // 送信失敗の場合
    //     } else {
    //         console.log("リッチメニュー切り替えに失敗しました。", JSON.stringify(response));  // コンソールにエラーメッセージを確認出力
    //     }
    // });

    const client = new line.Client({
        channelAccessToken: lineAccessToken
    });

    client.linkRichMenuToUser(userId, richMenuName);

}