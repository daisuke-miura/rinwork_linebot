

exports.getSelectMenuJson = function (replyToken) {
    // TODO : メニュー一覧をテーブル管理にして、店舗ごとに出しわけるようにする

    // sendData = {
    //     "replyToken": replyToken, // 返信する時に必要なトークン
    //     "messages": [{            // 送るメッセージ
    //         "type": "template",
    //         "altText": "plan_template",
    //         "template": {
    //             "type": "buttons",
    //             "text": "プランを選択してください",
    //             "actions": [
    //                 {
    //                     "type": "postback",
    //                     "label": "カットトリートメント",
    //                     "data": "cut"
    //                 },
    //                 {
    //                     "type": "postback",
    //                     "label": "カラートリートメント",
    //                     "data": "color"
    //                 },
    //                 {
    //                     "type": "postback",
    //                     "label": "パーマトリートメント",
    //                     "data": "perm"
    //                 },
    //                 {
    //                     "type": "postback",
    //                     "label": "ストレートトリートメント",
    //                     "data": "straight"
    //                 }
    //             ]
    //         }
    //     }]
    // };

    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "template",
            "altText": "reservation_menu_template",
            "template": {
                "type": "carousel",
                "columns": [
                    {
                        "text": "rin.のカットトリートメント",
                        "actions": [
                            {
                                "type": "postback",
                                "label": "このメニューで予約",
                                "data": "cut"
                            }
                        ]
                    },
                    {
                        "text": "髪本来の綺麗を取り戻す\nカラートリートメント",
                        "actions": [
                            {
                                "type": "postback",
                                "label": "このメニューで予約",
                                "data": "color"
                            }
                        ]
                    },
                    {
                        "text": "髪本来の綺麗を取り戻す\nパーマトリートメント",
                        "actions": [
                            {
                                "type": "postback",
                                "label": "このメニューで予約",
                                "data": "perm"
                            }
                        ]
                    },
                    {
                        "text": "髪本来の綺麗を取り戻す\nストレートトリートメント",
                        "actions": [
                            {
                                "type": "postback",
                                "label": "このメニューで予約",
                                "data": "straight"
                            }
                        ]
                    }
                ]
            }
        }]
    }
    return repJson;
}

exports.getSelectDateJson = function (replyToken) {
    let dt = new Date()
    dt.setDate(dt.getDate() + 1);
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "template",
            "altText": "datetime_picker",
            "template": {
                "type": "buttons",
                "title": "予約日時",
                "text": "以下より選択してください。",
                "actions": [
                    {
                        "type": "datetimepicker",
                        "label": "日時を選択してください。",
                        "data": "setdate",
                        "mode": "date",
                        "initial": dt.toFormat("YYYY-MM-DD"),
                        "min": dt.toFormat("YYYY-MM-DD")
                    }
                ]
            }
        }]
    };
    return repJson;
}

exports.getSelectTimeJson = function (replyToken, reservableTimeArray) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            // ----- クイックリプライバージョン -----
            "type": "text",
            "text": "開始時間を選択してください。",
            "quickReply": {
                "items": []
            }
        }]
    };
    for (let i = 0; i < reservableTimeArray.length; i++) {
        // 選択肢を13個以上表示できないので、
        // 13個目の場合は「→」（続きへ）ボタンに設定してbreak
        if (i === 12){
            let item = {
                "type": "action",
                "action": {
                    "type": "postback",
                    "label": "→",
                    "data": "settime_next"
                }
            }
            repJson["messages"][0]["quickReply"]["items"].push(item);
            break;
        }

        let reservableTime = reservableTimeArray[i];
        let item = {
            "type": "action",
            "action": {
                "type": "postback",
                "label": reservableTime,
                "data": "settime_" + reservableTime
            }
        }
        repJson["messages"][0]["quickReply"]["items"].push(item);
    }
    return repJson;
}

exports.getSelectTimePrevJson = function (replyToken, reservableTimeArray) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            // ----- クイックリプライバージョン -----
            "type": "text",
            "text": "開始時間を選択してください。",
            "quickReply": {
                "items": []
            }
        }]
    };
    // for (const reservableTime of reservableTimeArray) {
    for (let i = 0; i < reservableTimeArray.length; i++) {
        // 選択肢を13個以上表示できないので、
        // 13個目の場合は「→」（続きへ）ボタンに設定してbreak
        if (i === 12){
            let item = {
                "type": "action",
                "action": {
                    "type": "postback",
                    "label": "→",
                    "data": "settime_next"
                }
            }
            repJson["messages"][0]["quickReply"]["items"].push(item);
            break;
        }

        let reservableTime = reservableTimeArray[i];
        let item = {
            "type": "action",
            "action": {
                "type": "postback",
                "label": reservableTime,
                "data": "settime_" + reservableTime
            }
        }
        repJson["messages"][0]["quickReply"]["items"].push(item);
    }
    return repJson;
}

exports.getSelectTimeNextJson = function (replyToken, reservableTimeArray) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            // ----- クイックリプライバージョン -----
            "type": "text",
            "text": "開始時間を選択してください。",
            "quickReply": {
                "items": []
            }
        }]
    };
    console.log("★★★", reservableTimeArray);
    // 13個目以降の予約可能時間を設定
    for (let i = 11; i < reservableTimeArray.length; i++) {
        console.log("予約可能時間リスト設定", i);
        // 1つ目は「←」（戻る）ボタンに設定してbreak
        if (i === 11){
            let item = {
                "type": "action",
                "action": {
                    "type": "postback",
                    "label": "←",
                    "data": "settime_prev"
                }
            }
            console.log(item);
            repJson["messages"][0]["quickReply"]["items"].push(item);
            continue;
        }

        let reservableTime = reservableTimeArray[i];
        let item = {
            "type": "action",
            "action": {
                "type": "postback",
                "label": reservableTime,
                "data": "settime_" + reservableTime
            }
        }
        repJson["messages"][0]["quickReply"]["items"].push(item);
    }
    return repJson;
}

exports.getConfirmationJson = function (replyToken, operationType, reservationDate, reservationTime, menuName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "template",
            "altText": "reservation_completed_template",
            "template": {
                "type": "confirm",
                "actions": [
                    {
                        "type": "postback",
                        "label": "はい",
                        "data": "reservation_completed"
                    },
                    {
                        "type": "postback",
                        "label": "いいえ",
                        "data": "reservation_suspend"
                    }
                ],
                "text": "以下の内容で" + operationType + "してよろしいですか？\n"
                    + "【日付】  " + reservationDate + "\n"
                    + "【時間】  " + reservationTime + "\n"
                    + "【プラン】" + menuName
            }
        }]
    };
    return repJson;
}

exports.getCompleteJson = function (replyToken, completeOpName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": completeOpName + "が完了しました。"
        }]
    }
    return repJson;
}

/**
 * 予約変更を確定させていいかの確認メッセージ
 * 
 */
exports.getChangePermissionJson = function (replyToken, reservStartDateTime, menuName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "template",
            "altText": "reservation_change_permission_template",
            "template": {
                "type": "confirm",
                "actions": [
                    {
                        "type": "postback",
                        "label": "はい",
                        "data": "reservation_change"
                    },
                    {
                        "type": "postback",
                        "label": "いいえ",
                        "data": "reservation_change_suspend"
                    }
                ],
                "text": "以下の予約を変更しますか？\n"
                    + "【日付】  " + reservStartDateTime.toFormat("YYYY-MM-DD") + "\n"
                    + "【時間】  " + (("0" + reservStartDateTime.getHours()).slice(-2)) + ":" + (("0" + reservStartDateTime.getMinutes()).slice(-2)) + "\n"
                    + "【プラン】" + menuName
            }
        }]
    };
    return repJson;
}

/**
 * 予約キャンセルを確定させていいかの確認メッセージ
 * 
 */
exports.getCancelPermissionJson = function (replyToken, reservStartDateTime, menuName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "template",
            "altText": "reservation_cancel_permission_template",
            "template": {
                "type": "confirm",
                "actions": [
                    {
                        "type": "postback",
                        "label": "はい",
                        "data": "reservation_cancel_completed"
                    },
                    {
                        "type": "postback",
                        "label": "いいえ",
                        "data": "reservation_cancel_suspend"
                    }
                ],
                "text": "以下の予約をキャンセルしますか？\n"
                    + "【日付】  " + reservStartDateTime.toFormat("YYYY-MM-DD") + "\n"
                    + "【時間】  " + (("0" + reservStartDateTime.getHours()).slice(-2)) + ":" + (("0" + reservStartDateTime.getMinutes()).slice(-2)) + "\n"
                    + "【プラン】" + menuName
            }
        }]
    };
    return repJson;
}
/**
 * 予約内容の確認メッセージ
 * 
 */
exports.getCheckReservationJson = function (replyToken, reservStartDateTime, menuName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": "以下の内容で予約されています\n"
            + "【日付】  " + reservStartDateTime.toFormat("YYYY-MM-DD") + "\n"
            + "【時間】  " + (("0" + reservStartDateTime.getHours()).slice(-2)) + ":" + (("0" + reservStartDateTime.getMinutes()).slice(-2)) + "\n"
            + "【プラン】" + menuName
        }]
    };
    return repJson;
}


exports.getNoReservationJson = function (replyToken) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": "予約が見つかりませんでした。"
        }]
    };
    return repJson;
}

exports.getSuspensionJson = function (replyToken, cancelOpName) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": cancelOpName + "を中断しました。"
        }]
    };
    return repJson;
}

exports.getErrorJson = function (replyToken) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": "エラーが発生しました。申し訳ありませんが、最初から操作し直してください。"
        }]
    };
    return repJson;
}

exports.getIllegalOperationJson = function (replyToken) {
    let repJson = {
        "replyToken": replyToken,
        "messages": [{
            "type": "text",
            "text": "不正な入力です。直接文字入力せず、メニューから操作してください。"  // 文字
        }]
    };
    return repJson;
}