/*************************
  ScheduleCalc.js
 *************************/

/** GoogleCalendarAPI操作Util */
const googleCalendarAPI = require("./GoogleCarendarApiOperator")

/**
 * 予約可能時間のリストを取得
 * 
 * @param {*} userId 
 * @param {*} date      予約日（YYYY-MM-DD）
 */
exports.getReservableTimeList = async function (userId, date) {
    return new Promise((resolve, reject) => {
        // 開店時間、閉店時間を設定（日付部分は使用しない）
        var openTime = new Date(date);
        var closeTime = new Date(date);
        // TODO: 店ごとの営業開始、終了時間を取得できるようにする
        openTime.setHours(9, 00, 00);
        closeTime.setHours(18, 00, 00);

        // 0時～24時までの30分毎の空き時間を表す配列を作成
        // 開店時間は"○"、閉店時間帯は"×"を設定
        var freeTimeList = [];
        for (let i = 0; i < 48; i++) {
            let hours = i / 2;
            // let minutes = i % 2 * 30;
            // 開店時間以降、且つ閉店時間以前なら"○"を設定
            // if (((openTime.getHours() + openTime.getMinutes() / 60) <= (hours + minutes / 60))
            //     && ((hours + minutes / 60) < (closeTime.getHours() + closeTime.getMinutes() / 60))) {
            if (((openTime.getHours() + openTime.getMinutes() / 60) <= hours)
                && (hours < (closeTime.getHours() + closeTime.getMinutes() / 60))) {
                console.log("hours", hours);
                // console.log("minutes", minutes);
                freeTimeList.push("○");
                // 上記意外なら"×"を設定
            } else {
                freeTimeList.push("×");
            }
        }

        googleCalendarAPI.getCalEventByDateTime(userId, date, openTime, closeTime)
            .then((calEvents) => {
                if (calEvents) {
                    // 取得した予定の数だけループ
                    for (const calEvent of calEvents) {
                        // 予約開始、終了日時を取得
                        let eventStartTime = new Date(calEvent["start"]["dateTime"]);
                        let eventEndTime = new Date(calEvent["end"]["dateTime"]);
                        // 日本時間に補正
                        eventStartTime.setHours(eventStartTime.getHours() + 9);
                        eventEndTime.setHours(eventEndTime.getHours() + 9);
                        // 予約開始、終了時間のインデックス番号を取得
                        let eventStartTimeIdx = eventStartTime.getHours() * 2 + eventStartTime.getMinutes() / 30;
                        let eventEndTimeIdx = eventEndTime.getHours() * 2 + eventEndTime.getMinutes() / 30;
                        // 予約がある時間帯は"×"に書き換え
                        for (let i = eventStartTimeIdx; i < eventEndTimeIdx; i++) {
                            freeTimeList[i] = "×"
                        }
                    }
                }
                console.log("freeTimeList 予約時間反映", freeTimeList);

                // TODO：　メニューによって施術時間を出しわける
                // "○"が4回続く（2時間の空きがある）インデックス番号をすべて取得
                let operationTime = 4;
                // let regexp = new RegExp('○{' + operationTime + '}');

                // 予約可能時間インデックス番号リスト
                // let reservableTimeIdxList = freeTimeList.join().matchAll(regexp);
                let reservableTimeIdxList = [];
                for (let i = 0; i < freeTimeList.length - operationTime; i++) {
                    let checkArray = [freeTimeList[i], freeTimeList[i + 1], freeTimeList[i + 2], freeTimeList[i + 3]]
                    if (!checkArray.includes("×")) {
                        reservableTimeIdxList.push(i);
                    }
                }
                console.log("予約可能時間インデックス番号リスト", reservableTimeIdxList);

                // 予約可能時間インデックス番号リストから予約可能時間リストを作成
                // 時：（予約可能時間インデックス番号）/　2
                // 分：（予約可能時間インデックス番号）%　2 * 30
                let reservableTimeList = [];
                for (const reservableTime of reservableTimeIdxList) {
                    console.log("reservableTime", reservableTime);
                    let reservableTime_hours = Math.floor(reservableTime / 2);
                    let reservableTime_minutes = reservableTime % 2 * 30;
                    reservableTimeList.push(
                        (("0" + reservableTime_hours).slice(-2)) + ":" + (("0" + reservableTime_minutes).slice(-2)));

                }
                console.log("予約可能時間リスト", reservableTimeList);
                resolve(reservableTimeList);
            })
            .catch((err) => {
                console.log(err.message);
                reject(err);
            })
    })
}
